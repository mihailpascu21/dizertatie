#include <SPI.h>

#ifdef ALTERNATE_PINS
  #define VSPI_MISO   2
  #define VSPI_MOSI   4
  #define VSPI_SCLK   0
  #define VSPI_SS     33

  #define HSPI_MISO   26
  #define HSPI_MOSI   27
  #define HSPI_SCLK   25
  #define HSPI_SS     32
#else
  #define VSPI_MISO   MISO  //19
  #define VSPI_MOSI   MOSI  //23
  #define VSPI_SCLK   SCK   //18
  #define VSPI_SS     SS    //5

  #define HSPI_MISO   12
  #define HSPI_MOSI   13
  #define HSPI_SCLK   14
  #define HSPI_SS     15
#endif

#define NOT_HOLD  27

#define T_STORE 10000u
#define T_RESTORE 200u

typedef enum {
  WREN = 0x06,
  WRDI = 0x04,
  WRITE = 0x02,
  READ = 0x03,
  SECURE_WRITE = 0x12,
  SECURE_READ = 0x13,
  WRSR = 0x01,
  RDSR = 0x05,
  STORE = 0x08,
  RECALL = 0x09,
  WRNUR = 0xC2,
  RDNUR = 0xC3,
  HIBERNATE = 0xB9
} SRAM48LM01_Instruction;

typedef enum{
  BSY_BIT_MASK = 1,
  WEL_BIT_MASK = 2,
  BP0_BIT_MASK = 4,
  BP1_BIT_MASK = 8,
  SWM_BIT_MASK = 16,
  ASE_BIT_MASK = 64
}SRAM48LM01_StatusRegister_Masks;

static boolean isHibernate = false;

static const int spiClk = 1000000; // 1 MHz
//uninitalised pointers to SPI objects
SPIClass * vspi = NULL;

/****************************************
 * Prototypes
 */
void testFunctionality();

void setup() {
  delay(1000);
   //initialise the instance of the SPIClass attached to HSPI
  vspi = new SPIClass(VSPI);
  
  #ifndef ALTERNATE_PINS
  //initialise vspi with default pins
  //SCLK = 18, MISO = 19, MOSI = 23, SS = 5
  vspi->begin();
#else
  //alternatively route through GPIO pins of your choice
  vspi->begin(VSPI_SCLK, VSPI_MISO, VSPI_MOSI, VSPI_SS); //SCLK, MISO, MOSI, SS
#endif

  //set up slave select pins as outputs as the Arduino API
  //doesn't handle automatically pulling SS low
  pinMode(VSPI_SS, OUTPUT); //VSPI SS
  pinMode(NOT_HOLD, OUTPUT); //NOT_HOLD 
  digitalWrite(NOT_HOLD, HIGH);
  
  //the SRAM wakes-up from hibernat on power cycle
  isHibernate = false;
  testFunctionality();
}

void loop() {
  delay(1000);
} 
