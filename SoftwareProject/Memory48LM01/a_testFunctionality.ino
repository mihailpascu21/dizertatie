void testFunctionality()
{
//  testWriteEnabledDisabledCommands();
//  testReadCommand();
  testWriteCommand();
//  testWriteRegisterCommand();
//  testSTORE();
//  testRECALL();
//  testReadUserSpace();
//  testWriteUserSpace();
//  testEERAMBackupOnWriteUserSpace();
//  testHIBERNATE();

  testSecureWrite();
  testSecureRead();
}

void testWriteEnabledDisabledCommands()
{
  VspiSendCommand_ReadStatusRegister();
  VspiSendCommand_WriteEnabled();
  VspiSendCommand_ReadStatusRegister();
  VspiSendCommand_WriteDisabled();
  VspiSendCommand_ReadStatusRegister();
}

void testReadCommand()
{
  //ToDo: Varianta in care sa am burst la readByte (size si cat anume vreau sa citesc)
  uint32_t address = 0x00000000;
  Serial.begin(115200);
  Serial.print("The value at address ");
  Serial.print(address);
  Serial.print(" is ");
  Serial.println(VspiSendCommand_Read(address));
  address = 0x00000001;
  Serial.print("The value at address ");
  Serial.print(address);
  Serial.print(" is ");
  Serial.println(VspiSendCommand_Read(address));
  address = 0x00000002;
  Serial.print("The value at address ");
  Serial.print(address);
  Serial.print(" is ");
  Serial.println(VspiSendCommand_Read(address));
  address = 0x00000003;
  Serial.print("The value at address ");
  Serial.print(address);
  Serial.print(" is ");
  Serial.println(VspiSendCommand_Read(address));
  delay(2000);
  Serial.end();
}

void testWriteCommand()
{
  uint32_t address = 0x00000000;
  uint32_t dataByte = 0xA5A5A5A5;
  VspiSendCommand_Write(address,(uint8_t*)&dataByte,4u);
  testReadCommand();
}

void testWriteRegisterCommand()
{
  VspiSendCommand_ReadStatusRegister();
  VspiSendCommand_WriteEnabled();
  VspiSendCommand_ReadStatusRegister();
  VspiSendCommand_WriteStatusRegister((byte)0x00);
  VspiSendCommand_ReadStatusRegister();
}

void testSTORE()
{
  testReadCommand();
  VspiSendCommand_Store();
  while(deviceIsBusy())
  {
    delay(100);
  }
  testWriteCommand();
}

void testRECALL()
{
  testSTORE();
  VspiSendCommand_Recall();
  while(deviceIsBusy())
  {
    delay(100);
  }
  testReadCommand();
}

void testHIBERNATE()
{
  VspiSendCommand_Hybernate();
  VspiSendCommand_WakeUpDevice();
  VspiSendCommand_WriteEnabled();
  VspiSendCommand_ReadStatusRegister();
  VspiSendCommand_Hybernate();
}

void testReadUserSpace()
{
  uint8_t dataArray[16]={0x0};
  VspiSendCommand_ReadNonvolatileUserSpace(dataArray);
  Serial.begin(115200);
  Serial.print("The value in userSpace is ");
  for(uint8_t i=0; i<16; i++)
    Serial.println(dataArray[0]);
  delay(2000);
  Serial.end();
}

void testWriteUserSpace()
{
  uint8_t dataArray[16];
  for(uint8_t i=0;i<16u;i++)
    dataArray[i]=0xA5;
  testReadUserSpace();
  VspiSendCommand_WriteNonvolatileUserSpace(dataArray);
  testReadUserSpace();
}

void testEERAMBackupOnWriteUserSpace()
{
  uint8_t dataArray[16];
  for(uint8_t i=0;i<16u;i++)
    dataArray[i]=0xA5;
  testReadUserSpace();
  VspiSendCommand_WriteNonvolatileUserSpace(dataArray);
  VspiSendCommand_Store();
}

void testSecureWrite()
{ /* ToDo: SecureWrite */}

void testSecureRead()
{ /* ToDo: SecureRead */}
