void setupLocalWebPage(void)
{
  FunctionInitSPIFFS();
  FunctionInitWiFiAP();
  FunctionInitAsyncServer();
}

static void FunctionInitSPIFFS(void)
{
  /* Initialize SPIFFS */
  if(!SPIFFS.begin(true)){
    Serial.println("An Error has occurred while mounting SPIFFS");
    Serial.println("Restarting in 15 seconds");
    delay(15000);  
    ESP.restart();
  }
}
static void FunctionInitWiFiAP(void)
{
  /* Set up the AP Wi-Fi network with SSID and password */
  Serial.print("Setting AP (Access Point)…");
  WiFi.mode(WIFI_MODE_APSTA);
  /* Remove the password parameter, if you want the AP (Access Point) to be open */
//  WiFi.softAP(ssid, password);
  WiFi.softAP(ssid);
  /* Print ESP32 Local IP Address */
  IPAddress IP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(IP);
}

static void FunctionInitAsyncServer(void)
{
  /* Route for root / web page */
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/startPage.html", String());
  });
  
  server.on("/get", HTTP_GET, [](AsyncWebServerRequest *request){
    String inputSsid;
    String inputPass;
    if (request->hasParam(PARAM_INPUT_1)) {
      inputSsid = request->getParam(PARAM_INPUT_1)->value();
    }
    if (request->hasParam(PARAM_INPUT_2)) {
      inputPass = request->getParam(PARAM_INPUT_2)->value();
    }
    inputSsid.toCharArray(WLAN_SSID,50);
    inputPass.toCharArray(WLAN_PASS,50);

    startConnectionToWiFi = true;
    
    request->send(SPIFFS, "/connect.html", String());
  });

  server.on("/done", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/done.html", String());
  });

  // Handle Web Server Events
  events.onConnect([](AsyncEventSourceClient *client){
    // send event with message "hello!", id current millis
    // and set reconnect delay to 1 second
    client->send("hello", NULL, millis(), 10000);
  });
  server.addHandler(&events);
  
  /* Start server */
  server.begin();
}
