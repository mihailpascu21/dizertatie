//pins definition
#define VSPI_MISO   MISO  //19
#define VSPI_MOSI   MOSI  //23
#define VSPI_SCLK   SCK   //18
#define VSPI_SS     SS    //5
#define NOT_HOLD  27

//timeout definitions
#define T_STORE 10000u
#define T_RESTORE 200u

#define MAX_ADDRESS (uint32_t)131072
