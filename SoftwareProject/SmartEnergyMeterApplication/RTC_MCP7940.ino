void setupRTC(void)
{
  Wire.begin();                // join i2c bus (address optional for master)
//  startOscillator();         // this is only needed to run once 

  timeKeeper.calendar.date=0u;
  timeKeeper.calendar.month=0u;
  timeKeeper.calendar.year=0u;
  timeKeeper.timeClock.hour=0u;
  timeKeeper.timeClock.minute=0u;
  timeKeeper.timeClock.second=0u;
  timeKeeper.timeClock.hourFormat12=0u;
  timeKeeper.timeClock.hourPM=0u;
}

void getTimeStamp(void)
{
  readYear(&timeKeeper.calendar.year);
  readMonth(&timeKeeper.calendar.month);
  readDate(&timeKeeper.calendar.date);
  readHours(&timeKeeper.timeClock.hour);
  readMinutes(&timeKeeper.timeClock.minute);
  readSeconds(&timeKeeper.timeClock.second);
  timeKeeper.timeClock.hourFormat12 = is12HoursFormat();
  timeKeeper.timeClock.hourPM = isHourPM();
  printTime();
}

/***************************************************
 * Public functinality used just in hole project
 ***************************************************/
void startOscillator()
{
  uint8_t dataByte = 0x00;
  uint8_t bitST = readByteFromRTC(RTC_SEC)>>7u;
  if(0u == bitST)
  {
    dataByte |= 1u<<7u;
    writeByteToRTC(RTC_SEC, dataByte);
  }
  else
  {
    //oscillator is already started
  }
}

void stopOscillator()
{
  uint8_t dataByte = 0x00;
  dataByte &= ~(1u<<7u);
  writeByteToRTC(RTC_SEC, dataByte);
}

int writeSeconds(uint8_t seconds)
{
  if(seconds > 59u)
    return -1;
  uint8_t dataByte = convertDecimalToBCD(seconds);
  uint8_t bitST = readByteFromRTC(RTC_SEC)>>7u;
  dataByte |= bitST<<7u;
  writeByteToRTC(RTC_SEC, dataByte);
  return 0;
}

void readSeconds(uint8_t *seconds)
{
  uint8_t dataByte = 0x00;
  dataByte = readByteFromRTC(RTC_SEC);
  *seconds = convertBCDToDecimal(dataByte,MASK_3BITS);
}

int writeMinutes(uint8_t minutes)
{
  if(minutes > 59u)
    return -1;
  uint8_t dataByte = convertDecimalToBCD(minutes);
  writeByteToRTC(RTC_MIN, dataByte);
  return 0;
}

void readMinutes(uint8_t *minutes)
{
  uint8_t dataByte = 0x00;
  dataByte = readByteFromRTC(RTC_MIN);
  *minutes = convertBCDToDecimal(dataByte,MASK_3BITS);
}


int writeHours(uint8_t hours)
{
  uint8_t dataByte = convertDecimalToBCD(hours);
  uint8_t BCDHourRegister = readByteFromRTC(RTC_HOUR);
  uint8_t bitAM_PM = (BCDHourRegister >> 5u) & MASK_1BIT;
  uint8_t bitHourFormat = (BCDHourRegister >> 6u) & MASK_1BIT;
  
  if(bitHourFormat)
  {
    if(hours > 12u)
    {
      return -1;
    }
  }
  else
  {
    if(hours > 24u)
    {
      return -1;
    }
  }
  
  dataByte |= bitAM_PM<<5u;
  dataByte |= bitHourFormat<<6u;
  writeByteToRTC(RTC_HOUR, dataByte);
  return 0;
}

void readHours(uint8_t *hours)
{
  uint8_t dataByte = 0x00;
  dataByte = readByteFromRTC(RTC_HOUR);
  
  if(is12HoursFormat)
    *hours = convertBCDToDecimal(dataByte,MASK_1BIT);
  else
    *hours = convertBCDToDecimal(dataByte,MASK_2BITS);
}

void set12HoursFormat(boolean format)
{
  uint8_t dataByte = 0x00;
  dataByte = readByteFromRTC(RTC_HOUR);

  if(format)
    dataByte |= (1<<6u);
  else
    dataByte &= ~(1<<6u);
  writeByteToRTC(RTC_HOUR, dataByte);  
}

boolean is12HoursFormat()
{
  uint8_t dataByte = 0x00;
  dataByte = readByteFromRTC(RTC_HOUR);
  dataByte = (dataByte>>7u) & MASK_1BIT;
  return (boolean)dataByte;
}

void setPMBit(boolean format)
{
  uint8_t dataByte = 0x00;
  dataByte = readByteFromRTC(RTC_HOUR);

  if(format)
    dataByte |= (1<<5u);
  else
    dataByte &= ~(1<<5u);
  writeByteToRTC(RTC_HOUR, dataByte);  
}

boolean isHourPM()
{
  uint8_t dataByte = 0x00;
  dataByte = readByteFromRTC(RTC_HOUR);
  if(is12HoursFormat())
    dataByte = (dataByte>>6u) & MASK_1BIT;
  else
    dataByte = 0x00;
  return (boolean)dataByte;
}

int writeWeekDay(uint8_t weekDay)
{
  if((weekDay > 7u) || (weekDay<=0))
    return -1;
  uint8_t wkDayByte = readByteFromRTC(RTC_WKDAY) >> 3u;
  uint8_t dataByte = convertDecimalToBCD(weekDay);
  dataByte |= wkDayByte << 3u;
  writeByteToRTC(RTC_WKDAY, dataByte);
  return 0;
}

void readWeekDay(uint8_t *weekDay)
{
  uint8_t dataByte = 0x00;
  dataByte = readByteFromRTC(RTC_WKDAY);
  *weekDay = convertBCDToDecimalOneDigit(dataByte,MASK_3BITS);
}

int writeDate(uint8_t date)
{
  if(date > 31u)
    return -1;
  uint8_t dataByte = convertDecimalToBCD(date);
  writeByteToRTC(RTC_DATE, dataByte);
  return 0;
}

void readDate(uint8_t *date)
{
  uint8_t dataByte = 0x00;
  dataByte = readByteFromRTC(RTC_DATE);
  *date = convertBCDToDecimal(dataByte,MASK_2BITS);
}

int writeMonth(uint8_t month)
{
  if(month > 12u)
    return -1;
  uint8_t dataByte = convertDecimalToBCD(month);
  uint8_t bitLPYR = readByteFromRTC(RTC_MTH)>>5u;
  dataByte |= bitLPYR<<5u;
  writeByteToRTC(RTC_MTH, month);
  return 0;
}

void readMonth(uint8_t *month)
{
  uint8_t dataByte = 0x00;
  dataByte = readByteFromRTC(RTC_MTH);
  *month = convertBCDToDecimal(dataByte,MASK_1BIT);
}

int writeYear(uint8_t year)
{
  if(year > 99u)
    return -1;
  uint8_t dataByte = convertDecimalToBCDYear(year);
  writeByteToRTC(RTC_YEAR, dataByte);
  return 0;
}

void readYear(uint8_t *year)
{
  uint8_t dataByte = 0x00;
  dataByte = readByteFromRTC(RTC_YEAR);
  *year = convertBCDToDecimal(dataByte,MASK_4BITS);
}

/***************************************************
 * Static functinality used just inside this file
 ***************************************************/
static void writeByteToRTC(uint8_t registerAddress, uint8_t dataByte)
{
  Wire.beginTransmission(MCP7940_ADDRESS); // transmit to device #0x6F address from Datasheet page 10
  Wire.write(byte(registerAddress)); 
  Wire.write(byte(dataByte)); 
  Wire.endTransmission();      // stop transmitting
}

static uint8_t readByteFromRTC(uint8_t registerAddress)
{
  Wire.beginTransmission(MCP7940_ADDRESS); // transmit to device #0x6F address from Datasheet page 10
  Wire.write(byte(registerAddress)); 
  Wire.endTransmission();      // stop transmitting

  uint8_t dataByte = 0x00;
  Wire.requestFrom((uint16_t)MCP7940_ADDRESS, (uint8_t)sizeof(uint8_t), (boolean)true);
  if (sizeof(uint8_t) <= Wire.available()) { // if one byte is received
    dataByte = Wire.read();  // receive byte
  }
  return dataByte;
}


/***************************************************
 * Utility functions
 ***************************************************/
static uint8_t convertDecimalToBCD(uint8_t decimalValue)
{
  uint8_t BCDValue = 0x0;
  uint8_t decimalValueLocal = decimalValue;
  BCDValue |= (decimalValueLocal%10u) & MASK_4BITS;
  decimalValueLocal /= 10u;
  BCDValue |= ((decimalValueLocal%10u)& MASK_3BITS) << 4u;
  printConversions(decimalValue, BCDValue);
  return BCDValue;
}

static uint8_t convertDecimalToBCDYear(uint8_t decimalValue)
{
  uint8_t BCDValue = 0x0;
  uint8_t decimalValueLocal = decimalValue;
  BCDValue |= (decimalValueLocal%10u) & MASK_4BITS;
  decimalValueLocal /= 10u;
  BCDValue |= ((decimalValueLocal%10u)& MASK_4BITS) << 4u;
  printConversions(decimalValue, BCDValue);
  return BCDValue;
}

static uint8_t convertBCDToDecimal(uint8_t BCDValue, uint8_t maskSecondDigit)
{
  uint8_t decimalValue = 0x0; 
  decimalValue |= BCDValue & MASK_4BITS;
  decimalValue += ((BCDValue>>4u) & maskSecondDigit) * 10u;
  printConversions(decimalValue, BCDValue);
  return decimalValue;
}

static uint8_t convertBCDToDecimalOneDigit(uint8_t BCDValue, uint8_t maskSecondDigit)
{
  uint8_t decimalValue = 0x0; 
  decimalValue |= BCDValue & maskSecondDigit;
  printConversions(decimalValue, BCDValue);
  return decimalValue;
}

static void printConversions(uint8_t decimalValue, uint8_t BCDValue)
{
  //printDecimal(decimalValue);
  //printBCDByte(BCDValue);
}
static void printDecimal(uint8_t decimalValue)
{
  Serial.print("Decimal = ");
  Serial.println(decimalValue);
}

static void printBCDByte(uint8_t BCDValue)
{
  Serial.print("BCD = ");
  Serial.println(BCDValue,BIN);
}

static void printTime(void)
{
  Serial.print("The timestamp got from RTC is: ");
  if(timeKeeper.calendar.date<10)
    Serial.print("0");
  Serial.print(timeKeeper.calendar.date);
  Serial.print(".");
  if(timeKeeper.calendar.month<10)
    Serial.print("0");
  Serial.print(timeKeeper.calendar.month);
  Serial.print(".20");
  if(timeKeeper.calendar.year<10)
    Serial.print("0");
  Serial.print(timeKeeper.calendar.year);
  Serial.print(" --- ");
  if(timeKeeper.timeClock.hour<10)
    Serial.print("0");
  Serial.print(timeKeeper.timeClock.hour);
  Serial.print(":");
  if(timeKeeper.timeClock.minute<10)
    Serial.print("0");
  Serial.print(timeKeeper.timeClock.minute);
  Serial.print(":");
  if(timeKeeper.timeClock.second<10)
    Serial.print("0");
  Serial.print(timeKeeper.timeClock.second);
  if(timeKeeper.timeClock.hourFormat12)
    Serial.print(timeKeeper.timeClock.hourPM?"PM":"AM");
  Serial.println();
}
