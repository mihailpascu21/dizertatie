/**********************************
 *  ADD THE HEADER
 **********************************/
#define LED_RED 26

void setup() {
  pinMode(LED_RED, OUTPUT);
  Serial.begin(115200);
  
  // Connect to Wi-Fi
  Serial.println(); Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WLAN_SSID);
  
  WiFi.begin(WLAN_SSID, WLAN_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP Address is: "); Serial.println(WiFi.localIP());

  mqtt.subscribe(&ledControl);
}

void loop() {
  // put your main code here, to run repeatedly:
  MQTT_connect();

  Adafruit_MQTT_Subscribe *subscription;

  while((subscription = mqtt.readSubscription(5000)))
  {
    if(subscription == &ledControl)
    {
      Serial.print(F("Got: "));
      Serial.println((char *)ledControl.lastread);

      if(!strcmp((char*) ledControl.lastread, "ON"))
      {
        digitalWrite(LED_RED, HIGH);
      }
      else
      {
        digitalWrite(LED_RED, LOW);
      }
    }
  }

  if(!temperature.publish((float)random(30)))
  {
    Serial.println(F("Failed!"));
  }
  else
  {
    Serial.println(F("OK!"));
  }
  
}


void MQTT_connect(void)
{
  int8_t ret;

  //Stop if already connected
  if(mqtt.connected())
  {
    return;
  }

  Serial.print("Connecting to MQTT... ");

  uint8_t retries = 3;

  while((ret = mqtt.connect()) != 0)  //connect will return 0 for connected
  {
    Serial.println(mqtt.connectErrorString(ret));
    Serial.println("Retrying MQTT connection in 5 seconds...");
    mqtt.disconnect();

    delay(5000);

    retries--; 
  }
}
