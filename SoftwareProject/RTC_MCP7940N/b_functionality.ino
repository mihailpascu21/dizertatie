/***************************************************
 * Public functinality used just in hole project
 ***************************************************/
void startOscillator()
{
  uint8_t dataByte = 0x00;
  uint8_t bitST = readByteFromRTC(RTC_SEC)>>7u;
  if(0u == bitST)
  {
    dataByte |= 1u<<7u;
    writeByteToRTC(RTC_SEC, dataByte);
  }
  else
  {
    //oscillator is already started
  }
}

void stopOscillator()
{
  uint8_t dataByte = 0x00;
  dataByte &= ~(1u<<7u);
  writeByteToRTC(RTC_SEC, dataByte);
}

int writeSeconds(uint8_t seconds)
{
  if(seconds > 59u)
    return -1;
  uint8_t dataByte = convertDecimalToBCD(seconds);
  uint8_t bitST = readByteFromRTC(RTC_SEC)>>7u;
  dataByte |= bitST<<7u;
  writeByteToRTC(RTC_SEC, dataByte);
  return 0;
}

void readSeconds(uint8_t *seconds)
{
  uint8_t dataByte = 0x00;
  dataByte = readByteFromRTC(RTC_SEC);
  *seconds = convertBCDToDecimal(dataByte,MASK_3BITS);
}

int writeMinutes(uint8_t minutes)
{
  if(minutes > 59u)
    return -1;
  uint8_t dataByte = convertDecimalToBCD(minutes);
  writeByteToRTC(RTC_MIN, dataByte);
  return 0;
}

void readMinutes(uint8_t *minutes)
{
  uint8_t dataByte = 0x00;
  dataByte = readByteFromRTC(RTC_MIN);
  *minutes = convertBCDToDecimal(dataByte,MASK_3BITS);
}


int writeHours(uint8_t hours)
{
  uint8_t dataByte = convertDecimalToBCD(hours);
  uint8_t BCDHourRegister = readByteFromRTC(RTC_HOUR);
  uint8_t bitAM_PM = (BCDHourRegister >> 5u) & MASK_1BIT;
  uint8_t bitHourFormat = (BCDHourRegister >> 6u) & MASK_1BIT;
  
  if(bitHourFormat)
  {
    if(hours > 12u)
    {
      return -1;
    }
  }
  else
  {
    if(hours > 24u)
    {
      return -1;
    }
  }
  
  dataByte |= bitAM_PM<<5u;
  dataByte |= bitHourFormat<<6u;
  writeByteToRTC(RTC_HOUR, dataByte);
  return 0;
}

void readHours(uint8_t *hours)
{
  uint8_t dataByte = 0x00;
  dataByte = readByteFromRTC(RTC_HOUR);
  
  if(is12HoursFormat)
    *hours = convertBCDToDecimal(dataByte,MASK_1BIT);
  else
    *hours = convertBCDToDecimal(dataByte,MASK_2BITS);
}

void set12HoursFormat(boolean format)
{
  uint8_t dataByte = 0x00;
  dataByte = readByteFromRTC(RTC_HOUR);

  if(format)
    dataByte |= (1<<6u);
  else
    dataByte &= ~(1<<6u);
  writeByteToRTC(RTC_HOUR, dataByte);  
}

boolean is12HoursFormat()
{
  uint8_t dataByte = 0x00;
  dataByte = readByteFromRTC(RTC_HOUR);
  dataByte = (dataByte>>7u) & MASK_1BIT;
  return (boolean)dataByte;
}

void setPMBit(boolean format)
{
  uint8_t dataByte = 0x00;
  dataByte = readByteFromRTC(RTC_HOUR);

  if(format)
    dataByte |= (1<<5u);
  else
    dataByte &= ~(1<<5u);
  writeByteToRTC(RTC_HOUR, dataByte);  
}

boolean isHourPM()
{
  uint8_t dataByte = 0x00;
  dataByte = readByteFromRTC(RTC_HOUR);
  if(is12HoursFormat())
    dataByte = (dataByte>>6u) & MASK_1BIT;
  else
    dataByte = 0x00;
  return (boolean)dataByte;
}

/***************************************************
 * Static functinality used just inside this file
 ***************************************************/
static void writeByteToRTC(uint8_t registerAddress, uint8_t dataByte)
{
  Wire.beginTransmission(MCP7940_ADDRESS); // transmit to device #0x6F address from Datasheet page 10
  Wire.write(byte(registerAddress)); 
  Wire.write(byte(dataByte)); 
  Wire.endTransmission();      // stop transmitting
}

static uint8_t readByteFromRTC(uint8_t registerAddress)
{
  Wire.beginTransmission(MCP7940_ADDRESS); // transmit to device #0x6F address from Datasheet page 10
  Wire.write(byte(registerAddress)); 
  Wire.endTransmission();      // stop transmitting

  uint8_t dataByte = 0x00;
  Wire.requestFrom((uint16_t)MCP7940_ADDRESS, (uint8_t)sizeof(uint8_t), (boolean)true);
  if (sizeof(uint8_t) <= Wire.available()) { // if one byte is received
    dataByte = Wire.read();  // receive byte
  }
  return dataByte;
}
