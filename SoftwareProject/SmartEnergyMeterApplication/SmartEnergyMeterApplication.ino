#include <Wire.h> //library needed for the I2C communication with MCP7940 RTC
#include <SPI.h>  //library needed for the SPI communication with 48LM01 SRAM chip

//libraries needed for the connection to the Adafruit IoT Platform
#include <WiFi.h>
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"

//libraries needed for the local AsyncWebServer
#include "SPIFFS.h"
#include "ESPAsyncWebServer.h"

/* FreeRTOS defines */
#define PRO_CORE 0
#define APP_CORE 1

#define TICKS_DELAY 5

struct calendarStruct
{
  uint8_t date;
  uint8_t month;
  uint8_t year;
};

struct timeStruct
{
  uint8_t hour;
  uint8_t minute;
  uint8_t second;
  boolean hourFormat12;
  boolean hourPM;
};

typedef struct dateTimeStruct
{
  struct calendarStruct calendar;
  struct timeStruct     timeClock;
} dateTime;


typedef enum {
  WREN = 0x06,
  WRDI = 0x04,
  WRITE = 0x02,
  READ = 0x03,
  SECURE_WRITE = 0x12,
  SECURE_READ = 0x13,
  WRSR = 0x01,
  RDSR = 0x05,
  STORE = 0x08,
  RECALL = 0x09,
  WRNUR = 0xC2,
  RDNUR = 0xC3,
  HIBERNATE = 0xB9
} SRAM48LM01_Instruction;

typedef enum{
  BSY_BIT_MASK = 1,
  WEL_BIT_MASK = 2,
  BP0_BIT_MASK = 4,
  BP1_BIT_MASK = 8,
  SWM_BIT_MASK = 16,
  ASE_BIT_MASK = 64
}SRAM48LM01_StatusRegister_Masks;


int inputData=0u;
dateTime timeKeeper;
static boolean isHibernate = false;
static const int spiClk = 1000000; //1 MHz
SPIClass * vspi = NULL;

boolean dataAndTimestampRead = false;
boolean startConnectionToWiFi = false;
boolean connectedToWiFi = false;

void setup() {
  Serial.begin(115200);
  setupLocalWebPage();
  setupRTC();
  setupSRAM();
  setupRS485();

  xTaskCreatePinnedToCore(
  getInputDataFromRS485
  ,  "TaskToReadValueFromRS485"   /* A name just for humans */
  ,  1024   /* This stack size can be checked & adjusted by reading the Stack Highwater */
  ,  NULL
  ,  2      /* Priority, with 3 (configMAX_PRIORITIES - 1) being the highest, and 0 being the lowest. */
  ,  NULL 
  ,  PRO_CORE);

  xTaskCreatePinnedToCore(
  writeToMemory
  ,  "TaskToWriteToMemory"
  ,  1024   /* Stack size */
  ,  NULL
  ,  1      /* Priority */
  ,  NULL 
  ,  APP_CORE);

  xTaskCreatePinnedToCore(
  setupIoTPlatform
  ,  "TaskToConnectToIoTPlatform"
  ,  4096   /* Stack size */
  ,  NULL
  ,  1      /* Priority */
  ,  NULL 
  ,  PRO_CORE);

  xTaskCreatePinnedToCore(
  handleDataIoTPlatform
  ,  "TaskToWriteDataToIoTPlatform"
  ,  4096   /* Stack size */
  ,  NULL
  ,  2      /* Priority */
  ,  NULL 
  ,  APP_CORE);
} 


void loop() {
  /* do nothing */
}
