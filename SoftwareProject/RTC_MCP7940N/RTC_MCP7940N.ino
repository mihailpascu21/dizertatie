#include <Wire.h>

#define MCP7940_ADDRESS (byte)0x6F
#define MASK_1BIT (byte)0x01
#define MASK_2BITS (byte)0x03
#define MASK_3BITS (byte)0x07
#define MASK_4BITS (byte)0x0F


typedef enum {
  RTC_SEC = 0x0,
  RTC_MIN = 0x1,
  RTC_HOUR = 0x2,
  RTC_WKDAY = 0x3,
  RTC_DATE = 0x4,
  RTC_MTH = 0x5,
  RTC_YEAR = 0x6
} MCP7940_TimeAndDateRegs;

typedef enum {
  CONTROL = 0x7,
  OSCTRIM = 0x8
} MCP7940_ConfigAndTrimmingRegs;

typedef enum {
  ALM0_SEC = 0xA,
  ALM0_MIN = 0xB,
  ALM0_HOUR = 0xC,
  ALM0_WKDAY = 0xD,
  ALM0_DATE = 0xE,
  ALM0_MTH = 0xF
} MCP7940_Alarm0Regs;

typedef enum {
  ALM1_SEC = 0x11,
  ALM1_MIN = 0x12,
  ALM1_HOUR = 0x13,
  ALM1_WKDAY = 0x14,
  ALM1_DATE = 0x15,
  ALM1_MTH = 0x16
} MCP7940_Alarm1Regs;

typedef enum {
  PWRDN_MIN = 0x18,
  PWRDN_HOUR = 0x19,
  PWRDN_DATE = 0x1A,
  PWRDN_MTH = 0x1B
} MCP7940_PowerFailRegs;

typedef enum {
  PWRUP_MIN = 0x1C,
  PWRUP_HOUR = 0x1D,
  PWRUP_DATE = 0x1E,
  PWRUP_MTH = 0x1F
} MCP7940_PowerUpRegs;

typedef enum {
  SRAM = 0x20
} MCP7940_SRAM;

uint8_t reading = 0u;

void setup() {
  Wire.begin();                // join i2c bus (address optional for master)
    startOscillator();
//    stopOscillator();
  Serial.begin(115200);          // start serial communication at 115200bps

  //  Wire.beginTransmission(MCP7940_ADDRESS); // transmit to device #0x6F address from Datasheet page 10
  //  Wire.write(byte(0x03));
  //  Wire.write(byte(0x04));
  //  Wire.endTransmission();      // stop transmitting

  testFunctionality();

}

void loop() {
  //  // step 1: instruct sensor to read echoes
  //  Wire.beginTransmission((byte)0x6F); // transmit to device #0x6F address from Datasheet page 10
  //  Wire.write(byte(0x03));
  //  Wire.endTransmission();      // stop transmitting
  //
  //  Wire.requestFrom((uint16_t)0x6F, (uint8_t)sizeof(uint8_t), (boolean)true);
  //  if (1 <= Wire.available()) { // if one byte is received
  //    reading = Wire.read();  // receive byte
  //    Serial.println(reading);   // print the reading
  //  }

  //
  //  Wire.write(byte(0x03));      // sets register pointer to the command register (0x00)
  //  Wire.write(byte(0x50));      // command sensor to measure in "inches" (0x50)
  //  // use 0x51 for centimeters
  //  // use 0x52 for ping microseconds
  //  Wire.endTransmission();      // stop transmitting
  //  // step 2: wait for readings to happen
  //  delay(70);                   // datasheet suggests at least 65 milliseconds
  //  // step 3: instruct sensor to return a particular echo reading
  //  Wire.beginTransmission(112); // transmit to device #112
  //  Wire.write(byte(0x02));      // sets register pointer to echo #1 register (0x02)
  //  Wire.endTransmission();      // stop transmitting
  //  // step 4: request reading from sensor
  //  Wire.requestFrom(112, 2);    // request 2 bytes from peripheral device #112
  //  // step 5: receive reading from sensor
  //  if (2 <= Wire.available()) { // if two bytes were received
  //    reading = Wire.read();  // receive high byte (overwrites previous reading)
  //    reading = reading << 8;    // shift high byte to be high 8 bits
  //    reading |= Wire.read(); // receive low byte as lower 8 bits
  //    Serial.println(reading);   // print the reading
  //  }

  testReadSeconds();
  delay(1000);                  // wait a bit since people have to read the output :)
}
