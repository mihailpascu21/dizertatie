/***********************************************************
 * EXTERNAL FUNCTIONS (used just in the entire project)
 ***********************************************************/
 void VspiSendCommand_WriteEnabled() 
{
  VspiSendCommandNoData(WREN);
}

void VspiSendCommand_WriteDisabled() {
  VspiSendCommandNoData(WRDI);
}

void VspiSendCommand_Write(uint32_t address, uint8_t *dataByte, uint8_t dataSize) 
{
  VspiSendCommand_WriteEnabled();
  /*check that Write Enable Latch is set before writing */
  if(deviceIsReadyForWriting())
  {
    VspiSendCommandWithAddressAndData(WRITE, address, dataByte, dataSize);
  }
}

uint8_t VspiSendCommand_Read(uint32_t address) 
{
  uint8_t returnVal = 0xFF;
  if(deviceIsNotBusy())
    returnVal = VspiSendCommandWithAddressAndOneByteReturn(READ, address); 
  return returnVal;
}

void VspiSendCommand_SecureWrite()
{ /* ToDo: SecureWrite */}
void VspiSendCommand_SecureRead()
{ /* ToDo: SecureRead */}

void VspiSendCommand_WriteStatusRegister(uint8_t dataRegister) 
{
  VspiSendCommandWithData(WRSR,&dataRegister, sizeof(uint8_t));
}

uint8_t VspiSendCommand_ReadStatusRegister() 
{  
  return VspiSendCommandWithOneByteReturn(RDSR);
}

void VspiSendCommand_Store() 
{
  VspiSendCommandNoData(STORE);
}

void VspiSendCommand_Recall() 
{
  VspiSendCommandNoData(RECALL);
}

void VspiSendCommand_WriteNonvolatileUserSpace(uint8_t *dataArray) 
{
  VspiSendCommand_WriteEnabled();
  /*check that Write Enable Latch is set before writing */
  if(deviceIsReadyForWriting())
  {
    VspiSendCommandWithData(WRNUR, dataArray, sizeof(uint8_t)*16);
  }
}

void VspiSendCommand_ReadNonvolatileUserSpace(uint8_t* dataArray) 
{
  VspiSendCommandWithMultipleBytesReturn(RDNUR,dataArray, sizeof(uint8_t)*16);
}

void VspiSendCommand_Hybernate() 
{
  VspiSendCommandNoData(HIBERNATE);
  delayMicroseconds(T_STORE);
  isHibernate = true;
}

/* this is not a command, it is just one way of wakingUp the device from hibernation */
void VspiSendCommand_WakeUpDevice() 
{
  if(isHibernate)
  {
    digitalWrite(VSPI_SS, LOW); //pull SS low to prep other end for transfer
    delayMicroseconds(T_RESTORE);
    digitalWrite(VSPI_SS, HIGH); //pull SS low to prep other end for transfer
    //the setting of CS to LOW wakes-up the SRAM from hibernation after 200us
    isHibernate = false;
  }
}

/***********************************************
 * STATIC FUNCTIONS (used just in this tab)
 ***********************************************/
static boolean deviceIsReadyForWriting(void)
{
  return (1u == ((VspiSendCommand_ReadStatusRegister() & WEL_BIT_MASK)>>1u));
}

static boolean deviceIsBusy(void)
{
  return !deviceIsNotBusy();
}


static boolean deviceIsNotBusy(void)
{
  return (0u == ((VspiSendCommand_ReadStatusRegister() & BSY_BIT_MASK)>>0u));
}

static void VspiBeginCommunication()
{
  vspi->beginTransaction(SPISettings(spiClk, MSBFIRST, SPI_MODE0));
  digitalWrite(VSPI_SS, LOW); //pull SS low to prep other end for transfer
}

static void VspiEndCommunication()
{
  digitalWrite(VSPI_SS, HIGH); //pull SS high to signify end of data transfer
  vspi->endTransaction();
}

static void VspiSendCommandNoData(SRAM48LM01_Instruction command)
{
  VspiSendCommand_WakeUpDevice();
  VspiBeginCommunication();
  vspi->transfer((byte)command);
  VspiEndCommunication();
}

static void VspiSendCommandWithData(SRAM48LM01_Instruction command, uint8_t *dataArray, uint8_t dataSize)
{
  VspiSendCommand_WakeUpDevice();
  VspiBeginCommunication();
  vspi->transfer((byte)command);
  for(uint8_t i=0u;i<dataSize;i++)
    vspi->transfer((byte)dataArray[i]);   // send data
  VspiEndCommunication();
}

static uint8_t VspiSendCommandWithOneByteReturn(SRAM48LM01_Instruction command)
{
  uint8_t dataByte=0x00;
  VspiSendCommand_WakeUpDevice();
  VspiBeginCommunication();
  vspi->transfer((byte)command);
  vspi->transfer(&dataByte, sizeof(dataByte));       // receive data
  VspiEndCommunication();
  return dataByte;
}

static void VspiSendCommandWithMultipleBytesReturn(SRAM48LM01_Instruction command, uint8_t *dataOutputArray, uint8_t dataSize)
{
  VspiSendCommand_WakeUpDevice();
  VspiBeginCommunication();
  vspi->transfer((byte)command);
  for(uint8_t i=0; i< dataSize; i++)
    dataOutputArray[i] = vspi->transfer((byte)0xFF);
  VspiEndCommunication();
}


static void VspiSendCommandWithAddressAndData(SRAM48LM01_Instruction command, uint32_t address,const uint8_t *dataArray, uint8_t dataSize)
{
  VspiSendCommand_WakeUpDevice();
  VspiBeginCommunication();
  vspi->transfer((byte)command);
  vspi->transfer((byte)(address>>16));   // send address byte3 of address
  vspi->transfer((byte)(address>>8));    // send address byte2 of address
  vspi->transfer((byte)address);         // send address byte1 of address
  for(int i=0;i<dataSize;i++)
    vspi->transfer((byte)dataArray[i]);           // send data
  VspiEndCommunication();
}

static uint8_t VspiSendCommandWithAddressAndOneByteReturn(SRAM48LM01_Instruction command, uint32_t address )
{
  uint8_t dataByte=0x00;
  VspiSendCommand_WakeUpDevice();
  VspiBeginCommunication();
  vspi->transfer((byte)command);
  vspi->transfer((byte)(address>>16));   // send address byte3 of address
  vspi->transfer((byte)(address>>8));    // send address byte2 of address
  vspi->transfer((byte)address);         // send address byte1 of address
  vspi->transfer(&dataByte, sizeof(dataByte));       // send data
  VspiEndCommunication();
  return dataByte;
}
