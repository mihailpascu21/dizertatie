void setupRS485()
{
  // initialize digital pin NOTRE_DE as an output to control the Receiver/Transmiter status of the RS485 IC
  pinMode(MAX3483_NOTRE_DE_PIN, OUTPUT);
  digitalWrite(MAX3483_NOTRE_DE_PIN, LOW);   // MAX3483 Receiver Input Enable
  // serial begin to send(DI PIN) data to MAX3483 and receive data(RO PIN)
  Serial1.begin(115200,SERIAL_8N1,MAX3483_RO_PIN,MAX3483_DI_PIN);
}

void getInputDataFromRS485( void *pvParameters )
{
  (void) pvParameters;

  for (;;)
  {
    if(Serial1.available()>0)
    {
      if(Serial1.find("i"))
      {
        inputData=Serial1.parseInt(); 
        Serial.print("Data received from the the RS485 communication: ");
        Serial.print(inputData);  
        Serial.println();
        getTimeStamp();
        dataAndTimestampRead = true;
      }
    }
    /* TICKS_DELAY (one tick = 15ms) in between reads for stability */
    vTaskDelay(TICKS_DELAY*100);  
  }
}
