
void setupSRAM(void)
{
  //initialise the instance of the SPIClass attached to HSPI
  vspi = new SPIClass(VSPI);
  
  //initialise vspi with default pins
  //SCLK = 18, MISO = 19, MOSI = 23, SS = 5
  vspi->begin();

  //set up slave select pins as outputs as the Arduino API
  //doesn't handle automatically pulling SS low
  pinMode(VSPI_SS, OUTPUT); //VSPI SS
  pinMode(NOT_HOLD, OUTPUT); //NOT_HOLD 
  digitalWrite(NOT_HOLD, HIGH); 

  //the SRAM wakes-up from hibernate on power cycle
  isHibernate = false;

  writeAddressToMemory(0x03);
}

void writeToMemory( void *pvParameters )
{
  (void) pvParameters;
  
  for (;;)
  {
    if(true == dataAndTimestampRead)
    {
      uint32_t address=0x03;
      address = readAddressFromMemory();
      if(MAX_ADDRESS < (address+sizeof(dateTime)))
      {
        address = 0x03;
      }
      VspiSendCommand_Write(address, (uint8_t*)&timeKeeper, sizeof(dateTime));
      address = address + sizeof(dateTime);
      VspiSendCommand_Write(address, (uint8_t*)&inputData, sizeof(inputData));
      address = address + sizeof(inputData);
      writeAddressToMemory(address);
      dataAndTimestampRead = false;


      readFromMemory(); 
    }
    /* TICKS_DELAY (one tick = 15ms) in between reads for stability */
      vTaskDelay(TICKS_DELAY);  
  }
}


static void readFromMemory(void)
{
    uint32_t address=0x03;
    int dataReceived;
    address = readAddressFromMemory() - sizeof(dateTime) - sizeof(inputData);
    Serial.print("Value just send and written to memory at address ");\
    Serial.print(address);
    Serial.println(" is: ");
    Serial.print("Timestamp: ");
    for(int i=0;i<sizeof(dateTime);i++)
    {
      Serial.print(VspiSendCommand_Read(address));
      Serial.print(" ");
      address++;
    }
    dataReceived=     (VspiSendCommand_Read(address+3u)<<24u)|
                      (VspiSendCommand_Read(address+2u)<<16u)|
                      (VspiSendCommand_Read(address+1u)<<8u)|
                       VspiSendCommand_Read(address);
    address+=sizeof(inputData);
    Serial.println();
    Serial.print("Data: ");
    Serial.print(dataReceived);
    Serial.println();
}

void writeAddressToMemory(uint32_t address)
{
  VspiSendCommand_Write(0x00, (uint8_t*)&address, sizeof(address)-1u);
  //VspiSendCommand_WriteNonvolatileUserSpace((uint8_t*)&address, sizeof(address));
}

uint32_t readAddressFromMemory(void)
{
  uint32_t address =  (VspiSendCommand_Read(0x02)<<16u)|
                      (VspiSendCommand_Read(0x01)<<8u)|
                       VspiSendCommand_Read(0x00);
  //VspiSendCommand_ReadNonvolatileUserSpace((uint8_t*)&address, sizeof(address));
  return address;
}
/***********************************************************
 * EXTERNAL FUNCTIONS (used just in the entire project)
 ***********************************************************/
 void VspiSendCommand_WriteEnabled(void) 
{
  VspiSendCommandNoData(WREN);
}

void VspiSendCommand_WriteDisabled(void) {
  VspiSendCommandNoData(WRDI);
}

void VspiSendCommand_Write(uint32_t address, uint8_t *dataByte, uint8_t dataSize) 
{
  VspiSendCommand_WriteEnabled();
  /*check that Write Enable Latch is set before writing */
  if(deviceIsReadyForWriting())
  {
    VspiSendCommandWithAddressAndData(WRITE, address, dataByte, dataSize);
  }
}

uint8_t VspiSendCommand_Read(uint32_t address) 
{
  uint8_t returnVal = 0xFF;
  if(deviceIsNotBusy())
    returnVal = VspiSendCommandWithAddressAndOneByteReturn(READ, address); 
  return returnVal;
}

void VspiSendCommand_SecureWrite(void)
{ /* ToDo: SecureWrite */}
void VspiSendCommand_SecureRead(void)
{ /* ToDo: SecureRead */}

void VspiSendCommand_WriteStatusRegister(uint8_t dataRegister) 
{
  VspiSendCommandWithData(WRSR,&dataRegister, sizeof(uint8_t));
}

uint8_t VspiSendCommand_ReadStatusRegister(void) 
{  
  return VspiSendCommandWithOneByteReturn(RDSR);
}

void VspiSendCommand_Store(void) 
{
  VspiSendCommandNoData(STORE);
}

void VspiSendCommand_Recall(void) 
{
  VspiSendCommandNoData(RECALL);
}

void VspiSendCommand_WriteNonvolatileUserSpace(uint8_t *dataArray, uint8_t dataSize) 
{
  VspiSendCommand_WriteEnabled();
  /*check that Write Enable Latch is set before writing */
  if(dataSize <= 16u)
  {
    if(deviceIsReadyForWriting())
    {
      VspiSendCommandWithData(WRNUR, dataArray, dataSize);
    }
  }
}

void VspiSendCommand_ReadNonvolatileUserSpace(uint8_t* dataArray, uint8_t dataSize) 
{
  if(dataSize <= 16u)
  {
    VspiSendCommandWithMultipleBytesReturn(RDNUR,dataArray, dataSize);
  }
}

void VspiSendCommand_Hybernate(void) 
{
  VspiSendCommandNoData(HIBERNATE);
  delayMicroseconds(T_STORE);
  isHibernate = true;
}

/* this is not a command, it is just one way of wakingUp the device from hibernation */
void VspiSendCommand_WakeUpDevice(void) 
{
  if(isHibernate)
  {
    digitalWrite(VSPI_SS, LOW); //pull SS low to prep other end for transfer
    delayMicroseconds(T_RESTORE);
    digitalWrite(VSPI_SS, HIGH); //pull SS low to prep other end for transfer
    //the setting of CS to LOW wakes-up the SRAM from hibernation after 200us
    isHibernate = false;
  }
}

/***********************************************
 * STATIC FUNCTIONS (used just in this tab)
 ***********************************************/
static boolean deviceIsReadyForWriting(void)
{
  return (1u == ((VspiSendCommand_ReadStatusRegister() & WEL_BIT_MASK)>>1u));
}

static boolean deviceIsBusy(void)
{
  return !deviceIsNotBusy();
}


static boolean deviceIsNotBusy(void)
{
  return (0u == ((VspiSendCommand_ReadStatusRegister() & BSY_BIT_MASK)>>0u));
}

static void VspiBeginCommunication(void)
{
  vspi->beginTransaction(SPISettings(spiClk, MSBFIRST, SPI_MODE0));
  digitalWrite(VSPI_SS, LOW); //pull SS low to prep other end for transfer
}

static void VspiEndCommunication(void)
{
  digitalWrite(VSPI_SS, HIGH); //pull SS high to signify end of data transfer
  vspi->endTransaction();
}

static void VspiSendCommandNoData(SRAM48LM01_Instruction command)
{
  VspiSendCommand_WakeUpDevice();
  VspiBeginCommunication();
  vspi->transfer((byte)command);
  VspiEndCommunication();
}

static void VspiSendCommandWithData(SRAM48LM01_Instruction command, uint8_t *dataArray, uint8_t dataSize)
{
  VspiSendCommand_WakeUpDevice();
  VspiBeginCommunication();
  vspi->transfer((byte)command);
  for(uint8_t i=0u;i<dataSize;i++)
    vspi->transfer((byte)dataArray[i]);   // send data
  VspiEndCommunication();
}

static uint8_t VspiSendCommandWithOneByteReturn(SRAM48LM01_Instruction command)
{
  uint8_t dataByte=0x00;
  VspiSendCommand_WakeUpDevice();
  VspiBeginCommunication();
  vspi->transfer((byte)command);
  vspi->transfer(&dataByte, sizeof(dataByte));       // receive data
  VspiEndCommunication();
  return dataByte;
}

static void VspiSendCommandWithMultipleBytesReturn(SRAM48LM01_Instruction command, uint8_t *dataOutputArray, uint8_t dataSize)
{
  VspiSendCommand_WakeUpDevice();
  VspiBeginCommunication();
  vspi->transfer((byte)command);
  for(uint8_t i=0; i< dataSize; i++)
  {
    dataOutputArray[i] = vspi->transfer((byte)0xFF);
  }
  VspiEndCommunication();
}


static void VspiSendCommandWithAddressAndData(SRAM48LM01_Instruction command, uint32_t address,const uint8_t *dataArray, uint8_t dataSize)
{
  VspiSendCommand_WakeUpDevice();
  VspiBeginCommunication();
  vspi->transfer((byte)command);
  vspi->transfer((byte)(address>>16));   // send address byte3 of address
  vspi->transfer((byte)(address>>8));    // send address byte2 of address
  vspi->transfer((byte)address);         // send address byte1 of address
  for(int i=0;i<dataSize;i++)
    vspi->transfer((byte)dataArray[i]);           // send data
  VspiEndCommunication();
}

static uint8_t VspiSendCommandWithAddressAndOneByteReturn(SRAM48LM01_Instruction command, uint32_t address)
{
  uint8_t dataByte=0x00;
  VspiSendCommand_WakeUpDevice();
  VspiBeginCommunication();
  vspi->transfer((byte)command);
  vspi->transfer((byte)(address>>16));   // send address byte3 of address
  vspi->transfer((byte)(address>>8));    // send address byte2 of address
  vspi->transfer((byte)address);         // send address byte1 of address
  vspi->transfer(&dataByte, sizeof(dataByte));       // send data
  VspiEndCommunication();
  return dataByte;
}
