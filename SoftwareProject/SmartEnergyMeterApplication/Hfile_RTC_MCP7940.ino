#define MCP7940_ADDRESS (byte)0x6F
#define MASK_1BIT (byte)0x01
#define MASK_2BITS (byte)0x03
#define MASK_3BITS (byte)0x07
#define MASK_4BITS (byte)0x0F


typedef enum{
  RTC_SEC = 0x0,
  RTC_MIN = 0x1,
  RTC_HOUR = 0x2,
  RTC_WKDAY = 0x3,
  RTC_DATE = 0x4,
  RTC_MTH = 0x5,
  RTC_YEAR = 0x6
} MCP7940_TimeAndDateRegs;

typedef enum{
  CONTROL = 0x7,
  OSCTRIM = 0x8
} MCP7940_ConfigAndTrimmingRegs;

typedef enum{
  ALM0_SEC = 0xA,
  ALM0_MIN = 0xB,
  ALM0_HOUR = 0xC,
  ALM0_WKDAY = 0xD,
  ALM0_DATE = 0xE,
  ALM0_MTH = 0xF
} MCP7940_Alarm0Regs;

typedef enum{
  ALM1_SEC = 0x11,
  ALM1_MIN = 0x12,
  ALM1_HOUR = 0x13,
  ALM1_WKDAY = 0x14,
  ALM1_DATE = 0x15,
  ALM1_MTH = 0x16
} MCP7940_Alarm1Regs;

typedef enum{
  PWRDN_MIN = 0x18,
  PWRDN_HOUR = 0x19,
  PWRDN_DATE = 0x1A,
  PWRDN_MTH = 0x1B
} MCP7940_PowerFailRegs;

typedef enum{
  PWRUP_MIN = 0x1C,
  PWRUP_HOUR = 0x1D,
  PWRUP_DATE = 0x1E,
  PWRUP_MTH = 0x1F
} MCP7940_PowerUpRegs;

typedef enum{
  SRAM = 0x20
} MCP7940_SRAM;
