void testFunctionality()
{
//  testWriteSeconds();
//  testReadSeconds();
//  testWriteMinutes();
//  testReadMinutes();
//  testWriteHours();
//  testReadHours();
//ToDo:  testHourFormat();
//ToDo:  testAM_PM();
}

void testWriteSeconds()
{
  uint8_t seconds = 30u;
  writeSeconds(seconds);
  seconds = 59u;
  writeSeconds(seconds);
  seconds = 60u;
  writeSeconds(seconds);
  seconds = 0u;
  writeSeconds(seconds);
  seconds = 255u;
  writeSeconds(seconds);
  seconds = 24u;
  writeSeconds(seconds);
}

void testReadSeconds()
{
  uint8_t seconds = 0;
  readSeconds(&seconds);
}

void testWriteMinutes()
{
  uint8_t minutes = 30u;
  writeMinutes(minutes);
  minutes = 59u;
  writeMinutes(minutes);
  minutes = 60u;
  writeMinutes(minutes);
  minutes = 0u;
  writeMinutes(minutes);
  minutes = 255u;
  writeMinutes(minutes);
  minutes = 24u;
  writeMinutes(minutes);
}

void testReadMinutes()
{
  uint8_t minutes = 0u;
  readMinutes(&minutes);
}

void testWriteHours()
{
  uint8_t hours = 10u;
  writeHours(hours);
  hours = 12u;
  writeHours(hours);
  hours = 13;
  writeHours(hours);
  hours = 23u;
  writeHours(hours);
  hours = 24u;
  writeHours(hours);
  hours = 26u;
  writeHours(hours);
  hours = 2u;
  writeHours(hours);
}

void testReadHours()
{
  uint8_t hours = 0u;
  readHours(&hours);
}
