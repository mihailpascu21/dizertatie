/*
 * MAX3483CSA Driver
 */
 /*PLC pins*/
//#define MAX3483_NOTRE_DE_PIN (uint8_t)13
//#define MAX3483_RO_PIN (uint8_t)32
//#define MAX3483_DI_PIN (uint8_t)33
/* THT Board*/
#define MAX3483_NOTRE_DE_PIN (uint8_t)21
#define MAX3483_RO_PIN (uint8_t)22
#define MAX3483_DI_PIN (uint8_t)23


#define SENDER

// the setup function runs once when you press reset or power the board
void setup() {
#ifdef SENDER
  // initialize digital pin NOTRE_DE as an output to control the Receiver/Transmiter status of the RS485 IC
  pinMode(MAX3483_NOTRE_DE_PIN, OUTPUT);
  digitalWrite(MAX3483_NOTRE_DE_PIN, HIGH);   // MAX3483 Driver Output Enable
  // serial begin to send(DI PIN) data to MAX3483 and receive data(RO PIN)
  Serial1.begin(115200,SERIAL_8N1,MAX3483_RO_PIN,MAX3483_DI_PIN);
  Serial.begin(115200);
  
#else
  // initialize digital pin NOTRE_DE as an output to control the Receiver/Transmiter status of the RS485 IC
  pinMode(MAX3483_NOTRE_DE_PIN, OUTPUT);
  digitalWrite(MAX3483_NOTRE_DE_PIN, LOW);   // MAX3483 Receiver Input Enable
  // serial begin to send(DI PIN) data to MAX3483 and receive data(RO PIN)
  Serial2.begin(115200,SERIAL_8N1,MAX3483_RO_PIN,MAX3483_DI_PIN);
  Serial.begin(115200);
#endif
  
}

// the loop function runs over and over again forever
void loop() {
#ifdef SENDER
  int value = random(30);
  Serial1.print("i");
  Serial1.print(value); //send the data
  Serial1.print("f");
  Serial.println(value);

#else
  if(Serial2.available()>0)
    Serial.print("Message received from MAX3453:");    
  while(Serial2.available()>0)
    Serial.print((char)Serial2.read());    
  Serial.println();
#endif
  delay(5000);
}
