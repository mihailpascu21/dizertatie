#define RELAY (uint8_t)26
/**********************************
 *  ADD THE AIO KEY
 **********************************/
#define AIO_SERVER "io.adafruit.com"
#define AIO_SERVERPORT 1883
#define AIO_USERNAME  "MiGen97"
#define AIO_KEY  "aio_pArz64IQ7OX4fDtx9qKVkFEftVc7"

WiFiClient client;

//Setup MQTT client class by passing in the WiFi client and MQTT server and login details.
Adafruit_MQTT_Client mqtt(&client, AIO_SERVER, AIO_SERVERPORT, AIO_USERNAME, AIO_KEY);
Adafruit_MQTT_Subscribe relayControl = Adafruit_MQTT_Subscribe(&mqtt, "MiGen97/feeds/led");
Adafruit_MQTT_Subscribe iotSeconds = Adafruit_MQTT_Subscribe(&mqtt, "MiGen97/feeds/timestamp.seconds");
Adafruit_MQTT_Subscribe iotMinutes = Adafruit_MQTT_Subscribe(&mqtt, "MiGen97/feeds/timestamp.minutes");
Adafruit_MQTT_Subscribe iotHours = Adafruit_MQTT_Subscribe(&mqtt, "MiGen97/feeds/timestamp.hours");
Adafruit_MQTT_Subscribe iotDate = Adafruit_MQTT_Subscribe(&mqtt, "MiGen97/feeds/timestamp.date");
Adafruit_MQTT_Subscribe iotMonth = Adafruit_MQTT_Subscribe(&mqtt, "MiGen97/feeds/timestamp.month");
Adafruit_MQTT_Subscribe iotYear = Adafruit_MQTT_Subscribe(&mqtt, "MiGen97/feeds/timestamp.year");
Adafruit_MQTT_Publish energyConsumption = Adafruit_MQTT_Publish(&mqtt, "MiGen97/feeds/humidity");
Adafruit_MQTT_Publish instantEnergyConsumption = Adafruit_MQTT_Publish(&mqtt, "MiGen97/feeds/temperature");
