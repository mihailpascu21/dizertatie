#include <WiFi.h>
#include "SPIFFS.h"
#include <ESPAsyncWebServer.h>


const char* PARAM_INPUT_1 = "ssid";
const char* PARAM_INPUT_2 = "pass";
char WLAN_SSID[50] = "yourNetworkName";
char WLAN_PASS[50] =  "yourNetworkPass";
 
const char *ssid = "MyESP32AP";
//const char *password = "testpassword";


AsyncWebServer server(80);
 
 
void setup() {
  Serial.begin(115200);
  FunctionInitSPIFFS();
  FunctionInitWiFiAP();
  FunctionInitAsyncServer();
}
 
void loop() {}


void FunctionInitSPIFFS(void)
{
  /* Initialize SPIFFS */
  if(!SPIFFS.begin(true)){
    Serial.println("An Error has occurred while mounting SPIFFS");
    Serial.println("Restarting in 15 seconds");
    delay(15000);  
    ESP.restart();
  }
}

void FunctionInitWiFiAP(void)
{
  /* Set up the AP Wi-Fi network with SSID and password */
  Serial.print("Setting AP (Access Point)…");
  WiFi.mode(WIFI_MODE_APSTA);
  /* Remove the password parameter, if you want the AP (Access Point) to be open */
//  WiFi.softAP(ssid, password);
  WiFi.softAP(ssid);
  /* Print ESP32 Local IP Address */
  IPAddress IP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(IP);
}

void FunctionInitAsyncServer(void)
{
  /* Route for root / web page */
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/startPage.html", String());
  });
  
  server.on("/get", HTTP_GET, [](AsyncWebServerRequest *request){
    String inputSsid;
    String inputPass;
    if (request->hasParam(PARAM_INPUT_1)) {
      inputSsid = request->getParam(PARAM_INPUT_1)->value();
    }
    if (request->hasParam(PARAM_INPUT_2)) {
      inputPass = request->getParam(PARAM_INPUT_2)->value();
    }
    inputSsid.toCharArray(WLAN_SSID,50);
    inputPass.toCharArray(WLAN_PASS,50);

    connectToWiFi();
    
    request->send(SPIFFS, "/startPage.html", String());
  });
  
  /* Start server */
  server.begin();
}

void connectToWiFi(void)
{
  WiFi.mode(WIFI_STA);
  WiFi.begin(wifi_network_ssid,wifi_network_password);
  if (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("WiFi Failed!");
    return;
  }
  Serial.println();
  Serial.print("IP Address: ");
  Serial.println(WiFi.localIP());
}
/* Function used to deactivate the host server */
void FunctionWebServerStop(void)
{
  WiFi.softAPdisconnect (true);
  server.end();
}
