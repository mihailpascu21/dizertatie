static uint8_t convertDecimalToBCD(uint8_t decimalValue)
{
  uint8_t BCDValue = 0x0;
  uint8_t decimalValueLocal = decimalValue;
  BCDValue |= (decimalValueLocal%10u) & MASK_4BITS;
  decimalValueLocal /= 10u;
  BCDValue |= ((decimalValueLocal%10u)& MASK_3BITS) << 4u;
  printConversions(decimalValue, BCDValue);
  return BCDValue;
}

static uint8_t convertBCDToDecimal(uint8_t BCDValue, uint8_t maskSecondDigit)
{
  uint8_t decimalValue = 0x0; 
  decimalValue |= BCDValue & MASK_4BITS;
  decimalValue += ((BCDValue>>4u) & maskSecondDigit) * 10u;
  printConversions(decimalValue, BCDValue);
  return decimalValue;
}

static void printConversions(uint8_t decimalValue, uint8_t BCDValue)
{
  printDecimal(decimalValue);
  printBCDByte(BCDValue);
}
static void printDecimal(uint8_t decimalValue)
{
  Serial.print("Decimal = ");
  Serial.println(decimalValue);
}

static void printBCDByte(uint8_t BCDValue)
{
  Serial.print("BCD = ");
  Serial.println(BCDValue,BIN);
}
