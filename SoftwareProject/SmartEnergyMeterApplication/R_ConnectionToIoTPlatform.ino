void setupIoTPlatform( void *pvParameters )
{
  (void) pvParameters;
  
  for (;;)
  {
    if(true == startConnectionToWiFi)
    {
      startConnectionToWiFi = false;
      // Connect to Wi-Fi
      Serial.println(); Serial.println();
      Serial.print("Connecting to ");
      Serial.println(WLAN_SSID);
      
      WiFi.begin(WLAN_SSID, WLAN_PASS);
      while (WiFi.status() != WL_CONNECTED) {
        /* TICKS_DELAY (one tick = 15ms) in between reads for stability */
        vTaskDelay(TICKS_DELAY*30);  
        Serial.print(".");
      }
      connectedToWiFi = true;           
      events.send("connected",NULL,millis());
      
      Serial.println("");
      Serial.println("WiFi connected");  
      Serial.println("IP Address is: "); Serial.println(WiFi.localIP());

      mqtt.subscribe(&relayControl);
      mqtt.subscribe(&iotSeconds);
      mqtt.subscribe(&iotMinutes);
      mqtt.subscribe(&iotHours);
      mqtt.subscribe(&iotDate);
      mqtt.subscribe(&iotMonth);
      mqtt.subscribe(&iotYear);
      pinMode(RELAY,OUTPUT);
    
    }
    /* TICKS_DELAY (one tick = 15ms) in between reads for stability */
    vTaskDelay(TICKS_DELAY);  
  }
}

void handleDataIoTPlatform( void *pvParameters )
{
  (void) pvParameters;

  for (;;)
  {
    if(true == connectedToWiFi)
    {
      MQTT_connect();
      readDataFromIoTPlatform();
      writeDataToIoTPlatform();
    }
    /* TICKS_DELAY (one tick = 15ms) in between reads for stability */
    vTaskDelay(TICKS_DELAY);  
  }
}

static void readDataFromIoTPlatform(void)
{
  Adafruit_MQTT_Subscribe *subscription;

  while((subscription = mqtt.readSubscription(5000)))
  {
    readRelayControl(subscription);
    readIotSeconds(subscription);
    readIotMinutes(subscription);
    readIotHours(subscription);
    readIotDate(subscription);
    readIotMonth(subscription);
    readIotYear(subscription);
  }
}

static void readRelayControl(Adafruit_MQTT_Subscribe *subscription)
{
  if(subscription == &relayControl)
  {
    Serial.print(F("Received command from IoT Platform: "));
    Serial.println((char *)relayControl.lastread);
  
    if(!strcmp((char*) relayControl.lastread, "ON"))
    {
      digitalWrite(RELAY, HIGH);
    }
    else
    {
      digitalWrite(RELAY, LOW);
    }
  }
}

static void readIotSeconds(Adafruit_MQTT_Subscribe *subscription)
{
  if(subscription == &iotSeconds)
  {
    Serial.print(F("Received seconds from IoT Platform: "));
    Serial.println((char *) iotSeconds.lastread);
    writeSeconds(atoi((char *) iotSeconds.lastread));
  }
}

static void readIotMinutes(Adafruit_MQTT_Subscribe *subscription)
{
  if(subscription == &iotMinutes)
  {
    Serial.print(F("Received minutes from IoT Platform: "));
    Serial.println((char *) iotMinutes.lastread);
    writeMinutes(atoi((char *) iotMinutes.lastread));
  }
}

static void readIotHours(Adafruit_MQTT_Subscribe *subscription)
{
  if(subscription == &iotHours)
  {
    Serial.print(F("Received hours from IoT Platform: "));
    Serial.println((char *) iotHours.lastread);
    writeHours(atoi((char *) iotHours.lastread));
  }
}

static void readIotDate(Adafruit_MQTT_Subscribe *subscription)
{
  if(subscription == &iotDate)
  {
    Serial.print(F("Received date from IoT Platform: "));
    Serial.println((char *) iotDate.lastread);
    writeDate(atoi((char *) iotDate.lastread));
  }
}

static void readIotMonth(Adafruit_MQTT_Subscribe *subscription)
{
  if(subscription == &iotMonth)
  {
    Serial.print(F("Received date from IoT Platform: "));
    Serial.println((char *) iotMonth.lastread);
    writeMonth(atoi((char *) iotMonth.lastread));
  }
}

static void readIotYear(Adafruit_MQTT_Subscribe *subscription)
{
  if(subscription == &iotYear)
  {
    Serial.print(F("Received date from IoT Platform: "));
    Serial.println((char *) iotYear.lastread);
    writeYear(atoi((char *) iotYear.lastread));
  }
}

static void writeDataToIoTPlatform(void)
{
  float dataToSend = (float)inputData;
  if(!energyConsumption.publish(dataToSend))
  {
    Serial.println(F("Failed to transmit energy consumption to IoT Platform!"));
  }
  else
  {
    Serial.println(F("Energy consumption transmitted to IoT Platform!"));
  }
  if(!instantEnergyConsumption.publish(dataToSend))
  {
    Serial.println(F("Failed to transmit instant energy consumption to IoT Platform!"));
  }
  else
  {
    Serial.println(F("Instant energy consumption transmitted to IoT Platform!"));
  }
}

static void MQTT_connect(void)
{
  int8_t ret;

  //Stop if already connected
  if(mqtt.connected())
  {
    return;
  }

  Serial.print("Connecting to MQTT... ");

  uint8_t retries = 3;

  while((ret = mqtt.connect()) != 0)  //connect will return 0 for connected
  {
    Serial.println(mqtt.connectErrorString(ret));
    Serial.println("Retrying MQTT connection in 5 seconds...");
    mqtt.disconnect();

    /* TICKS_DELAY (one tick = 15ms) in between reads for stability */
    vTaskDelay(TICKS_DELAY*300);  

    retries--; 
  }
}
